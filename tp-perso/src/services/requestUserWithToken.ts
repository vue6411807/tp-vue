import { RoutesEnum } from '@/router/routes.enum'
import type { User } from '@/types/User'
import axios from 'axios'

export const requestUserWithToken = async (token: string): Promise<User> => {
  let newUser: User = {
    username: '',
    email: '',
    accountType: 'premium',
    image: ''
  }

  return axios
    .get(RoutesEnum.GetUserProfile, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      newUser = {
        ...newUser,
        username: response.data.display_name,
        email: response.data.email,
        accountType: response.data.product,
        image: response.data.images[0]?.url || ''
      }
      return newUser
    })
    .catch((error) => {
      console.error('Error while fetching user data', error)
      throw new Error('Error while fetching user data')
    })
}
