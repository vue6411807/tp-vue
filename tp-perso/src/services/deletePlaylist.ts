import axios from 'axios'
import { useTokenStore } from '@/stores/token'

const { token } = useTokenStore()

export const DeletePlaylist = async (playlistId: string): Promise<any> => {
  await axios
    .delete(`https://api.spotify.com/v1/playlists/${playlistId}/followers`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      return response
    })
    .catch((error) => {
      console.error('Error while deleting playlist', error)
      throw new Error('Error while deleting playlist')
    })
}
