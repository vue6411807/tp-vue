import { RoutesEnum } from '@/router/routes.enum'

export const authFlow = async () => {
  // Code Verifier
  const generateRandomString = (length: number) => {
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const values = crypto.getRandomValues(new Uint8Array(length))
    return values.reduce((acc, x) => acc + possible[x % possible.length], '')
  }
  const codeVerifier = generateRandomString(64)

  // Code Challenge
  const sha256 = async (plain: string) => {
    const encoder = new TextEncoder()
    const data = encoder.encode(plain)
    return window.crypto.subtle.digest('SHA-256', data)
  }

  const base64encode = (input: ArrayBuffer) => {
    return btoa(String.fromCharCode(...new Uint8Array(input)))
      .replace(/=/g, '')
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
  }

  const hashed = await sha256(codeVerifier)
  const codeChallenge = base64encode(hashed)
  const codeChallengeMethod = 'S256'

  // Request User Authorization
  const clientId = import.meta.env.VITE_SPOTIFY_CLIENT_ID
  const responseType = 'code'
  const redirectUri = RoutesEnum.SpotifyCallback
  // const state = '' // optional but recommended
  const scope =
    'playlist-modify-private streaming user-read-private user-read-email playlist-modify-public'

  // Request User Authorization
  const authUrl = new URL(RoutesEnum.AuthUrl)

  // generated in the previous step
  window.localStorage.setItem('code_verifier', codeVerifier)

  const params = {
    response_type: responseType,
    client_id: clientId,
    scope: scope,
    // state: state, // optional but recommended
    code_challenge_method: codeChallengeMethod,
    code_challenge: codeChallenge,
    redirect_uri: redirectUri
  }

  authUrl.search = new URLSearchParams(params).toString()
  window.location.href = authUrl.toString()
}

export const getTokenFromAuthCode = async (uriCode: string): Promise<string> => {
  // stored in the previous step
  if (!uriCode) return ''
  const codeVerifier = localStorage.getItem('code_verifier')
  if (!codeVerifier) {
    // TODO handle error
    return ''
  }

  const payload = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: new URLSearchParams({
      grant_type: 'authorization_code',
      code: uriCode,
      redirect_uri: RoutesEnum.SpotifyCallback,
      client_id: import.meta.env.VITE_SPOTIFY_CLIENT_ID,
      code_verifier: codeVerifier
    })
  }

  const body = await fetch(RoutesEnum.TokenUrl, payload)
  const response = await body.json()

  // token.setToken(response.access_token)
  return response.access_token
}
