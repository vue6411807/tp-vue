import axios from 'axios'
import { RoutesEnum } from '@/router/routes.enum'
import type { CreatePlaylistDto } from '@/dto/createPlaylistDto'
import { useUserStore } from '@/stores/user'
import { useTokenStore } from '@/stores/token'
import type { TrackType } from '@/types/SearchedItem'
import { usePlaylistsStore } from '@/stores/playlists'

const playlistStore = usePlaylistsStore()
const { username } = useUserStore()
const { token } = useTokenStore()

export const CreatePlaylist = async (createPlaylistDto: CreatePlaylistDto): Promise<string> => {
  // Data verification
  if (createPlaylistDto.name === '') {
    throw new Error('Playlist name is empty')
  }
  if (createPlaylistDto.description === '') {
    throw new Error('Playlist description is empty')
  }

  // Regex to replace {user_id} in the route
  const replaceRegex = new RegExp(/\{.*?\}/g)

  return axios
    .post(
      RoutesEnum.CreatePlaylist.replace(replaceRegex, username),
      {
        name: createPlaylistDto.name,
        description: createPlaylistDto.description,
        public: true
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    .then((response) => {
      // Return the playlist id
      playlistStore.addPlaylist(response.data)
      return response.data.id
    })
    .catch((error) => {
      console.error('Error while creating playlist', error)
      throw new Error('Error while creating playlist')
    })
}

export const AddTracksToThePlaylist = async (
  playlistId: string,
  seedTrackURI: string,
  seedArtistsURI: string[],
  time: string
): Promise<any> => {
  // Get Recommandation based on the ReferenceTrack
  const getTimeInMinutes = (time: string): number => {
    const [hours, minutes] = time.split(':')
    return parseInt(hours) * 60 + parseInt(minutes)
  }
  let recommandations: TrackType[] | null = null
  await axios
    .get('https://api.spotify.com/v1/recommendations', {
      headers: {
        Authorization: `Bearer ${token}`
      },
      params: {
        seed_tracks: seedTrackURI,
        seed_artists: seedArtistsURI.join(','),
        market: 'FR',
        limit: Math.floor(getTimeInMinutes(time) / 3)
      }
    })
    .then((response) => {
      recommandations = response.data.tracks
    })
    .catch((error) => {
      console.error('Error while getting recommandations', error)
      throw new Error('Error while getting recommandations')
    })

  console.log(recommandations)

  const tracksURI = recommandations!.map((track: TrackType) => track.uri)
  await axios
    .post(
      `https://api.spotify.com/v1/playlists/${playlistId}/tracks`,
      {
        uris: tracksURI
      },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      console.error('Error while adding tracks to the playlist', error)
      throw new Error('Error while adding tracks to the playlist')
    })
}
