import { RoutesEnum } from '@/router/routes.enum'
import type { SpotifyPlaylist } from '@/types/SpotifyPlaylist'
import axios from 'axios'
import { useTokenStore } from '@/stores/token'

const { token } = useTokenStore()

export const fetchPlaylist = async (): Promise<SpotifyPlaylist[]> => {
  return axios
    .get(RoutesEnum.GetUserPlaylists, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((response) => {
      console.log('response data', response.data.items)
      const regex = new RegExp('[c|C]hronolist')
      return response.data.items.filter((playlist: SpotifyPlaylist) => {
        return playlist.name.match(regex)
      })
    })
    .catch((error) => {
      console.error(error)
      throw new Error('Erreur lors de la récupération des playlists.')
    })
}
