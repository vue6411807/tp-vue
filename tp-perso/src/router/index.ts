import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('../views/DashboardView.vue'),
      children: [
        {
          path: '/profile',
          name: 'profile',
          component: () => import('../views/ProfileView.vue')
        },
        {
          path: '/playlists',
          name: 'playlists',
          component: () => import('../views/PlaylistsView.vue')
        },
        {
          path: '/playlist/:id',
          name: 'playlist',
          component: () => import('../views/PlaylistView.vue')
        },
        {
          path: '/create',
          name: 'create',
          component: () => import('../views/CreatePlaylistView.vue')
        }
      ]
    },
    {
      path: '/callback',
      name: 'callback',
      component: () => import('../views/CallBackView.vue')
    }
  ]
})

export default router
