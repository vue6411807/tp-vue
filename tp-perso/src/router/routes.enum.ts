export enum RoutesEnum {
  // Application
  Home = '/',
  Dashboard = '/dashboard',
  NotFound = '/not-found',

  // Spotify
  SpotifyCallback = 'https://spotify.jdev.com/callback',
  AuthUrl = 'https://accounts.spotify.com/authorize',
  TokenUrl = 'https://accounts.spotify.com/api/token',
  GetUserProfile = 'https://api.spotify.com/v1/me',
  GetUserPlaylists = 'https://api.spotify.com/v1/me/playlists',
  CreatePlaylist = 'https://api.spotify.com/v1/users/{user_id}/playlists',
  SearchItems = 'https://api.spotify.com/v1/search'
}
