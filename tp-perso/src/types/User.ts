export type User = {
  username: string
  email: string
  accountType: 'free' | 'premium'
  image: string
}
