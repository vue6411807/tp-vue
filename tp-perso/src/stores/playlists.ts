import { defineStore } from 'pinia'
import type { SpotifyPlaylist } from '../types/SpotifyPlaylist'
import { ref } from 'vue'

// TODO : Refacto the stores for components API
// https://pinia.vuejs.org/core-concepts/#Setup-Stores

export const usePlaylistsStore = defineStore('playlists', {
  state: () => {
    return {
      playlists: null as null | SpotifyPlaylist[],
      timer: ref(300) // 5 minutes
    }
  },
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    setPlaylists(playlists: SpotifyPlaylist[]) {
      this.playlists = playlists
    },
    addPlaylist(playlist: SpotifyPlaylist) {
      if (this.playlists === null) {
        this.playlists = [playlist]
      } else {
        this.playlists.push(playlist)
      }
    },
    // Reduce "timer" of 1 every second
    startTimer() {
      const intervalId = setInterval(() => {
        this.timer = this.timer - 1
        if (this.timer === 0) {
          clearInterval(intervalId) // Clear the interval when the timer reaches 0
          this.timer = 300
        }
      }, 1000)
    }
  }
})
