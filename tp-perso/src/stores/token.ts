import { defineStore } from 'pinia'

export const useTokenStore = defineStore('token', {
  state: () => {
    return { token: '', code: '' }
  },
  actions: {
    setToken(token: string) {
      this.token = token
    },
    setCode(code: string) {
      this.code = code
    }
  }
})
