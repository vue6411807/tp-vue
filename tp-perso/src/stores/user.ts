import { defineStore } from 'pinia'
import type { User } from '../types/User'

export const useUserStore = defineStore('user', {
  state: () => {
    return { username: '', email: '', accountType: 'premium', image: '' }
  },
  actions: {
    // TODO : Créer un switch pour passer de premium à free pour debug
    setUser(User: User) {
      this.username = User.username
      this.email = User.email
      this.accountType = User.accountType
      this.image = User.image
    }
  }

  // Composition API equivalent
  // const username = ref('')

  // function setUsername(newUsername: string) { username.value = newUsername }
  // export { username, setUsername }
})
