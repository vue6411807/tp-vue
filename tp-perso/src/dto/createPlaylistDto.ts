export interface CreatePlaylistDto {
  name: string
  description?: string
  duration: string
  style?: string
}
