/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        spotify: '#22C95C',
        veryLightGray: '#F5F5F5'
      },
      maxHeight: {
        '600px': '600px'
      },
      height: {
        '600px': '600px'
      },
      backgroundImage: {
        'playlist-placeholder': "url('/src/assets/undraw_blank_canvas_re_2hwy.svg')"
      }
    }
  },
  plugins: []
}
