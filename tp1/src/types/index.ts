export interface Todo {
  id: number;
  text: string;
  time: string;
  done: boolean;
  assignee?: string;
}
